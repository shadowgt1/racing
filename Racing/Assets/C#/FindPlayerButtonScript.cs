﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System;
using System.Threading;
using System.Text;
using SocketIO;
using System.Collections.Generic;
public class BadGuy : IComparable<BadGuy>
{
    public string name;
    public int power;

    public BadGuy(string newName, int newPower)
    {
        name = newName;
        power = newPower;
    }

    //This method is required by the IComparable
    //interface. 
    public int CompareTo(BadGuy other)
    {
        if (other == null)
        {
            return 1;
        }

        //Return the difference in power.
        return power - other.power;
    }
}
public class FindPlayerButtonScript : MonoBehaviour
{
    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("Local IP Address Not Found!");
    }


    public SocketIOComponent m_socket;
    Dictionary<string, string> m_data;
    void Start()
    {

        Debug.Log("DEBUG: " + GetLocalIPAddress());

        m_data = new Dictionary<string, string>();
        m_data["email"] = "some@email.com";
        m_data["pass"] = "1234";
        
        StartCoroutine(ConnectToServer());
        Debug.Log("DEBUG: m_socket.Emit(); end ");
    }

    public void OnReturnData(SocketIOEvent e)
    {
        Dictionary<string, string> values = e.data.ToDictionary();
        Debug.Log("DEBUG: User Return Data");
        Debug.Log("DEBUG: "+e.data.ToString());
        Debug.Log("DEBUG: "+ values["Vorname"] + values["Nachname"] +" ------ ");
    }

    public void OnDisconnect(SocketIOEvent e)
    {
        Debug.Log("DEBUG: User disconnected");
    }

    IEnumerator ConnectToServer()
    {
        yield return new WaitForSeconds(1);
        m_socket.On("RETURN_DATA", OnReturnData);

        yield return new WaitForSeconds(1);
        m_socket.Emit("USER_CONNECT");

        yield return new WaitForSeconds(1);
        m_socket.Emit("user:login", new JSONObject(m_data));



    }



    void Update()
    {

    }
}


/*

public class FindPlayerButtonScript : MonoBehaviour {


    
    // State object for receiving data from remote device.
    public class StateObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 256;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
    



    Socket m_MainSocket = null;

    bool socketReady = false;
    // The response from the remote device.
    private static String response = String.Empty;

    // ManualResetEvent instances signal completion.
    private static ManualResetEvent connectDone =
        new ManualResetEvent(false);
    private static ManualResetEvent sendDone =
        new ManualResetEvent(false);
    private static ManualResetEvent receiveDone =
        new ManualResetEvent(false);



    // Use this for initialization
    void Start () {
        
        try
        {
            
            Socket TcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint IpEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.7"), 30000);

            // Create a TCP/IP socket.
            TcpSocket.BeginConnect(IpEndPoint,new AsyncCallback(OnConnect), TcpSocket);
            connectDone.WaitOne(5000, false);
            Debug.Log("DEBUG: connectDone");
            // Connect to the remote endpoint.
            Receive(TcpSocket);
            Send(TcpSocket, "Test Send");
            //sendDone.WaitOne();


            // Receive the response from the remote device.
            
            receiveDone.WaitOne(5000, false);


        }
        catch(Exception e)
        {
            Debug.Log("DEBUG: "+ e);
        }
    }

    private void OnConnect(IAsyncResult i_ar)
    {
        
        Socket socket = (Socket)i_ar.AsyncState;
        if (socket.Connected)
        {
            Debug.Log("DEBUG: OnConnect is ok");
            //GetComponentInChildren<Text>().text = "Connect";
        }
        else
        {
            Debug.Log("DEBUG: OnConnect is false");
            //this.GetComponentInChildren<Text>().text = "test";
        }
    }
    
    private void Receive(Socket i_socket)
    {
        try
        {
            //this.GetComponentInChildren<Text>().text = "Connect";

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = i_socket;

            // Begin receiving the data from the remote device.
            i_socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReceiveCallback), state);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }


    private void ReceiveCallback(IAsyncResult ar)
    {
        Debug.Log("DEBUG: Receive is ok");
        try
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket client = state.workSocket;

            // Read data from the remote device.
            int bytesRead = client.EndReceive(ar);

            if (bytesRead > 0)
            {
               

                // There might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                // Get the rest of the data.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            else
            {
                Debug.Log("DEBUG: Receive is ok");
                // All the data has arrived; put it in response.
                if (state.sb.Length > 1)
                {
                    response = state.sb.ToString();
                }
                // Signal that all bytes have been received.
                receiveDone.Set();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    

    private void Send(Socket client, String data)
    {
        Debug.Log("DEBUG: try send");
        // Convert the string data to byte data using ASCII encoding.
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        // Begin sending the data to the remote device.
        client.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), client);
    }

    private void SendCallback(IAsyncResult ar)
    {
        Debug.Log("DEBUG: SendCallback ok");
        try
        {
            // Retrieve the socket from the state object.
            Socket client = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = client.EndSend(ar);
            Debug.Log("DEBUG: Sent {0} bytes to server."+ bytesSent);

            // Signal that all bytes have been sent.
            //sendDone.Set();
        }
        catch (Exception e)
        {
            Debug.Log("DEBUG: SendCallback Exception : " + e);
        }
    }



    // Update is called once per frame
    void Update () {
	
	}
}
*/